from unidecode import unidecode

var = input("Entrez une valeur: ")
var = var.replace(" ", "")
var = var.lower()
var = unidecode(var)
if(var == var[::-1]):
      print("L'entrée est un palindrome")
else:
      print("L'entrée n'est pas un palindrome")